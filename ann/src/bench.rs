use crate::annminer::{AnnMiner, AnnMinerS};
use anyhow::Result;
use utils::util;
use std::fmt::{Display, Formatter};
use std::ops::{AddAssign, Div};
use std::thread;
use std::time::Duration;

pub struct Bencher {
    repeats: u32,
    sampling: Duration,
}

impl Bencher {
    pub fn new(repeats: u32, sampling_ms: u64) -> Self {
        Self {
            repeats,
            sampling: Duration::from_millis(sampling_ms),
        }
    }

    pub fn bench_ann(&self, threads: usize) -> Result<()> {
        println!("Starting benchmark");
        println!("threads: {}", threads);
        let ann_miner = start_bench_ann(threads)?;

        // waits a bit to let the miner kick in.
        thread::sleep(Duration::from_millis(1000));
        ann_miner.hashes_per_second(); // this resets the inner counter.

        // we will just call this several times, and return their arithmetic average.
        // there probably are more advanced/accurate algorithms, like discarding the outliers,
        // using other kinds of average, etc., but we'll keep it simple.
        let mut result = BenchBlk::default();
        for i in 1..=self.repeats {
            thread::sleep(self.sampling);
            let partial = BenchBlk {
                hashes_per_second: ann_miner.hashes_per_second(),
                // other monitoring points in the future?
            };
            println!("{:2}. result: {}", i, partial);
            result += partial;
        }
        println!("average: {}", result / self.repeats);
        Ok(())
    }
}

/// Assembles all the infrastructure needed to ann mining, and starts it.
fn start_bench_ann(threads: usize) -> Result<AnnMiner> {
    let (ann_miner, _recv) = AnnMinerS::new(123, threads);
    println!("created miner");

    // recv is ignored, which closes the channel, but the target below is impossible, so it won't ever win.

    println!("starting mining");
    ann_miner.start([0u8; 32], 123, 0x03000001, None)?;
    Ok(ann_miner)
}

#[derive(Default)]
struct BenchBlk {
    hashes_per_second: f64,
}

impl AddAssign for BenchBlk {
    fn add_assign(&mut self, rhs: Self) {
        self.hashes_per_second += rhs.hashes_per_second
    }
}

impl Div<u32> for BenchBlk {
    type Output = BenchBlk;

    fn div(self, rhs: u32) -> Self::Output {
        BenchBlk {
            hashes_per_second: self.hashes_per_second / rhs as f64,
        }
    }
}

impl Display for BenchBlk {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}e/s", util::big_number(self.hashes_per_second))
    }
}
