// SPDX-License-Identifier: (LGPL-2.1-only OR LGPL-3.0-only)
use anyhow::{Result};
use clap::{App, Arg, SubCommand};
use termion::color;
use ann::annmine;
use utils::util;

const DEFAULT_ADDR: &str = "pkt1qw9cfwj7v4raruplvuys47fd7q0q2z8yve8l0tu";

fn warn_if_addr_default(payment_addr: &str) {
    if payment_addr == DEFAULT_ADDR {
        println!(
            "    {}Wallet belum di setting, mining ke wallet {}{}",
            color::Fg(color::LightYellow),
            DEFAULT_ADDR,
            color::Fg(color::Reset)
        );
    }
}

async fn ann_main(
    pools: Vec<String>,
    threads: usize,
    payment_addr: &str,
    uploaders: usize,
    timeout: usize,
    mine_old_anns: i32,
) -> Result<()> {
    warn_if_addr_default(payment_addr);
    let am = annmine::new(annmine::AnnMineCfg {
        pools,
        miner_id: util::rand_u32(),
        workers: threads,
        uploaders,
        pay_to: String::from(payment_addr),
        timeout,
        mine_old_anns,
    })
    .await?;
    annmine::start(&am).await?;

    util::sleep_forever().await
}

/// Benchmark encryptions per second in ann mining.
async fn bench_ann(threads: usize) -> Result<()> {
    const REPEAT: u32 = 10;
    const SAMPLING_MS: u64 = 5000;
    let bencher = ann::bench::Bencher::new(REPEAT, SAMPLING_MS);
    tokio::task::spawn_blocking(move || bencher.bench_ann(threads))
        .await
        .unwrap()
}

macro_rules! get_str {
    ($m:ident, $s:expr) => {
        if let Some(x) = $m.value_of($s) {
            x
        } else {
            return Ok(());
        }
    };
}

macro_rules! get_usize {
    ($m:ident, $s:expr) => {
        get_num!($m, $s, usize)
    };
}

macro_rules! get_num {
    ($m:ident, $s:expr, $n:ident) => {{
        let s = get_str!($m, $s);
        if let Ok(u) = s.parse::<$n>() {
            u
        } else {
            println!("Unable to parse argument {} as number [{}]", $s, s);
            return Ok(());
        }
    }};
}

async fn async_main(matches: clap::ArgMatches<'_>) -> Result<()> {
    sys::init();
    if let Some(ann) = matches.subcommand_matches("ann") {
    /// caranya custom pool "http://pool1,http://pool1" dst. JANGAN LUPA TANDA KOMA
        let default_pool: String = "https://stratum.zetahash.com".to_string();
        let pools = default_pool.split(',').map(|s| s.to_string()).collect();
        let payment_addr = DEFAULT_ADDR;
        let threads = get_usize!(ann, "threads");
        let uploaders = get_usize!(ann, "uploaders");
        let timeout = get_usize!(ann, "timeout");
        let mine_old_anns = get_num!(ann, "mineold", i32);
        ann_main(
            pools,
            threads,
            payment_addr,
            uploaders,
            timeout,
            mine_old_anns,
        )
        .await?;
    } else if let Some(bench) = matches.subcommand_matches("bench") {
        let threads = get_num!(bench, "threads", usize);
        bench_ann(threads).await?;
    }
    Ok(())
}

#[tokio::main]
async fn main() -> Result<()> {
    let cpus_str = format!("{}", num_cpus::get());
    let matches = App::new("packetcrypt")
        .setting(clap::AppSettings::ArgRequiredElseHelp)
        .subcommand(
            SubCommand::with_name("ann")
                .about("Run announcement miner")
                .arg(
                    Arg::with_name("threads")
                        .short("t")
                        .long("threads")
                        .help("Number of threads to mine with")
                        .default_value(&cpus_str)
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("uploaders")
                        .short("u")
                        .long("uploaders")
                        .help("Max concurrent uploads (per pool handler)")
                        .default_value("10")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("timeout")
                        .short("T")
                        .long("timeout")
                        .help("How long to wait for a reply before aborting an upload")
                        .default_value("30")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("mineold")
                        .short("m")
                        .long("mineold")
                        .help("How many blocks old to mine annoucements, -1 to let the pool decide")
                        .default_value("0"),
                )
        )
        .subcommand(
            SubCommand::with_name("bench")
            .about("Benchmark the encryptions per second of an announcement mining")
            .arg(
                Arg::with_name("threads")
                    .short("t")
                    .long("threads")
                    .help("Number of threads to mine with")
                    .default_value(&cpus_str)
                    .takes_value(true),
            )
        )
        .get_matches();

    async_main(matches).await
}
